# Setup
You can find documentation in the [ui5-toolkit-gulp](https://gitlab.com/smartshore/ui5/ui5-toolkit-gulp).
Documentation for advanced testing can be cound here [ui5-toolkit-gulp-test](https://gitlab.com/smartshore/ui5/ui5-toolkit-gulp-test)

## Prerequisites

1. [Install <img src="https://git-scm.com/images/logo@2x.png" alt="git" height="20px"/>](https://git-scm.com/downloads)
2. [Install <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Node.js_logo_2015.svg/1024px-Node.js_logo_2015.svg.png" alt="node.js" height="20px"/>](https://nodejs.org/en/download/)
3. [Install <img src="https://i.imgur.com/fMdIS4r.png" alt="VSCode" height="20px"/>](https://code.visualstudio.com/)

## Install gulp
Do ***once***.

> Once means you don't have to do it for every new project. It means once per computer. If you get a new laptop do it again. If not do not do it again.

    npm install gulp-cli -g

> <img src="https://raw.githubusercontent.com/gulpjs/artwork/master/gulp-2x.png" alt="drawing" height="32px"/> is a task runner which automates basic tasks during web dev for you.

## Install npm dependencies
Do ***once per project***.

    npm install

> <img src="https://discourse-cdn-sjc2.com/standard11/uploads/npm1/original/1X/5ebf6b88c16b17c03f3635e5334b440e8f67a234.png" alt="npm" height="16px"> is a package manager which hosts the plugins that gulp runs as tasks.
    
## Start application
After successful setup, the web application can be started via command line with

    gulp

The `default` task configures the necessary proxies, starts a web server that provides the files and monitors source code changes in the `webapp` folder.
When a change is made, the browser is automatically updated. A change to a JavaScript file is also tested with `ESLint`.

## Test application

    gulp test

## Deploy application into the SAP System

First configure your `configs/build.json`. Enter the connection data to the frontend server, the name of the BSP application (will be created if it doesn't exist), the name of the package (must already exist) and the transport request.

**Do not upload to the frontend server directly. Instead push your code to GitLab and let the CI pipeline do the magic**

## Stop running task

Press `Ctrl + C` in the command line where the current task is running.