sap.ui.define([
    "sap/ui/core/util/MockServer"
], function(MockServer) {
    "use strict";
    return {
        init: function() {
            const iDelay = 1000;

            // create
            const oMockServer = new MockServer({
                //rootUri: "http://mymockserver/com/"
                //rootUri: '' // TODO could be /sap/opu/odata/ something
                rootUri: "/sap/opu/odata/iwfnd/northwind/"
            });

            // configure
            /*eslint-disable */
            MockServer.config({
                autoRespond: true,
                autoRespondAfter: jQuery.sap.getUriParameters().get("serverDelay") || iDelay
            });

            // simulate
            const sPath = jQuery.sap.getModulePath("ch.berncon.generator.ui5.localService");
            oMockServer.simulate(sPath + "/metadata.xml", {
                mockdataBaseUrl: sPath + "/mockdata",
                generateMissingMockData: true
            });
            /*eslint-enable */
            // start
            oMockServer.start();
        }
    };
});
