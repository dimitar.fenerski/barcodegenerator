sap.ui.define(
  ["../controller/BaseController", "sap/m/MessageToast", "../model/formatter"],
  function(BaseController, MessageToast, formatter) {
    "use strict";

    return BaseController.extend(
      "ch.berncon.DF.BarcodeGenerator.BarcodeGenerator.controller.View2",
      {
        onInit: function() {
          BaseController.prototype.onInit.apply(this, arguments);
        },

        formatter: formatter,
        onClear: function() {
          this._setModel();
          // const aFieldNames = [
          //   "bCodeType",
          //   "GTIN",
          //   "chargeNr",
          //   "serialNr",
          //   "expDate",
          //   "prodDate"
          // ];
          // for (let i = 0; i < aFieldNames.length; i++) {
          //   this.byId(aFieldNames[i]).setValue("");
          // }
          // this.byId("size").setSelectedItemId("");
          // this.byId("barcodeImage").setSrc("");
          // const oDownloadButton = this.byId("downloadButton");
          // const oClearButton = this.byId("clearButton");
          // const oSubmitButton = this.byId("submitButton");
          // oDownloadButton.setEnabled(false);
          // oClearButton.setEnabled(false);
          // oSubmitButton.setEnabled(true);
        }
        // onDownload: function(oEvent) {
        //   const oDownloadLink = $("<a>")
        //     .attr(
        //       "href",
        //       "data:image/png;base64," + oEvent.getSource().data("B64")
        //     )
        //     .attr("download", "barcode.png")
        //     .appendTo("body");
        //   oDownloadLink[0].click();
        //   oDownloadLink.remove();
        // },

        // onSubmit: function () {
        //   // const oModel = this.getView().getModel();
        //   // const vlaue1 = oModel.getProperty("/type");
        //   // if (vlaue1 ) {
        //   // }
        //   // const aData = [];
        //   // const aFieldNames = [
        //   //   "bCodeType",
        //   //   "GTIN",
        //   //   "chargeNr",
        //   //   "serialNr",
        //   //   "expDate",
        //   //   "prodDate"
        //   // ];
        //   // let oPush = {};
        //   // let bEmpty = false;
        //   // for (let i = 0; i < aFieldNames.length; i++) {
        //   //   if (this.byId(aFieldNames[i]).getValue() != 0) {
        //   //     oPush[aFieldNames[i]] = this.byId(aFieldNames[i]).getValue();
        //   //     aData.push(oPush);
        //   //     oPush = {};
        //   //     if (this.byId(aFieldNames[i]).getValueState() === "Error") {
        //   //       this.byId(aFieldNames[i]).setValueState("None");
        //   //       this.byId(aFieldNames[i]).setValueStateText("");
        //   //     }
        //   //   } else {
        //   //     this.byId(aFieldNames[i]).setValueState("Error");
        //   //     this.byId(aFieldNames[i]).setValueStateText(
        //   //       "Feld nicht leer lassen"
        //   //     );
        //   //     bEmpty = true;
        //   //   }
        //   // }
        //   // if (bEmpty) {
        //   //   return;
        //   // }
        //   let sRequest = "/BarcodeSet(";
        //   const aODataProperties = [
        //     "Type",
        //     "Gtin",
        //     "Batch",
        //     "Sernr",
        //     "Expdt",
        //     "Mfddt"
        //   ];
        //   for (let i = 0; i < aODataProperties.length; i++) {
        //     sRequest +=
        //       aODataProperties[i] + "='" + aData[i][aFieldNames[i]] + "',";
        //   }
        //   sRequest += "Size='" + this.byId("size").getSelectedKey() + "')";
        //   const sDemoRequest =
        //     "/BarcodeSet(Type='DM_GS1',Gtin='781212121212',Batch='CHG001',Sernr='84001245687',Expdt=datetime'9999-12-31T00%3a00%3a00',Mfddt=datetime'9999-12-31T00%3a00%3a00',Size='1')";
        //   const oImage = this.byId("barcodeImage");
        //   const bDownload = this.byId("bDownload").getSelected();
        //   const oDownloadButton = this.byId("downloadButton");
        //   const oClearButton = this.byId("clearButton");
        //   const oSubmitButton = this.byId("submitButton");
        //   this.getOwnerComponent()
        //     .getModel()
        //     .read(sDemoRequest, {
        //       success: function(oData) {
        //         oImage.setSrc("data:image/png;base64," + oData.B64);
        //         if (bDownload) {
        //           const oDownloadLink = $("<a>")
        //             .attr("href", "data:image/png;base64," + oData.B64)
        //             .attr("download", "barcode.png")
        //             .appendTo("body");
        //           oDownloadLink[0].click();
        //           oDownloadLink.remove();
        //         }
        //         oDownloadButton.data("B64", oData.B64);
        //         oDownloadButton.setEnabled(true);
        //         oClearButton.setEnabled(true);
        //         oSubmitButton.setEnabled(false);
        //         MessageToast.show(oData.ErrorMsg);
        //       },
        //       error: function(oData) {
        //         MessageToast.show(oData.ErrorMsg);
        //       }
        //     });
        // },

        // _setModel: function() {
        //   this.getView()
        //     .getModel("view")
        //     .setData({
        //       type: "",
        //       gtin: "",
        //       chargnum: "",
        //       sernum: ""
        //     });
        // }
      }
    );
  }
);
