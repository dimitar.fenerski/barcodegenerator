sap.ui.define(["ch/berncon/generator/ui5/controller/BaseController",
    "ch/berncon/generator/ui5/model/formatter",
    "sap/m/MessageToast",
    "ch/berncon/generator/ui5/fragment/version/Version"

], function(BaseController,
    formatter, MessageToast, Version) {
    "use strict";
    return BaseController.extend("ch.berncon.generator.ui5.controller.Main", {
        formatter: formatter,

        onInit: function() {
            BaseController.prototype.onInit.apply(this, arguments);

            this.getRouter().getRoute("main").attachMatched(this._onRouteMatched, this);
            // delegate
            this._version = new Version(this);
        },
        _onRouteMatched: function(oEvent) {
            MessageToast.show("matched");
        },
        onPressButton: function() {
            const sMsg = this.getOwnerComponent().getManifestEntry("/sap.app/applicationVersion/version");
            MessageToast.show(sMsg);

        },
        onPress: function() {
            this._version.open();
        }
    });
});
