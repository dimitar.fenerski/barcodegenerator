sap.ui.define(["sap/ui/base/Object", "sap/m/MessageBox"], function(
  UI5Object,
  MessageBox
) {
  "use strict";

  return UI5Object.extend("de.otto.xss.bu.bu.ErrorHandler", {
    /*
     * Handles application errors by automatically attaching to the model events and displaying errors when needed.
     * @class
     * @param {sap.ui.core.UIComponent} oComponent reference to the app's component
     * @public
     * @alias
     */
    constructor: function(oComponent) {
      this._oComponent = oComponent;
      this._oResourceBundle = oComponent.getModel("i18n").getResourceBundle();

      this._bMessageOpen = false;
      this._sErrorText = this._oResourceBundle.getText("errorText");

      this._observerModel(oComponent.getModel());
    },

    _observerModel: function(oModel) {
      if (oModel) {
        oModel.attachMetadataFailed(function(oEvent) {
          const oParams = oEvent.getParameters();
          this._showMetadataError(oParams.response);
        }, this);

        oModel.attachRequestFailed(function(oEvent) {
          const oParams = oEvent.getParameters();
          const iNull = 0;
          const iHTTPError = 0;

          // An entity that was not found in the service is also throwing a 404 error in oData.
          // We already cover this case with a notFound target so we skip it here.
          // A request that cannot be sent to the server is a technical error that we have to handle though
          if (
            oParams.response.statusCode !== "404" ||
            (oParams.response.statusCode === iHTTPError &&
              oParams.response.responseText.indexOf("Cannot POST") === iNull)
          ) {
            this._showServiceError(oParams.response);
          }
        }, this);
      }
    },

    /**
     * Shows a {@link sap.m.MessageBox} when the metadata call has failed.
     * The user can try to refresh the metadata.
     * @param {string} sDetails a technical error to be displayed on request
     * @private
     */
    _showMetadataError: function(sDetails) {
      MessageBox.error("MetadataError", {
        id: "metadataErrorMessageBox",
        details: sDetails,

        actions: [MessageBox.Action.CLOSE],
        onClose: function(sAction) {
          if (sAction === MessageBox.Action.RETRY) {
            //this._oModel.refreshMetadata();
          }
        }
      });
    },

    /**
     * Shows a {@link sap.m.MessageBox} when a service call has failed.
     * Only the first error message will be display.
     * @param {string} sDetails a technical error to be displayed on request
     * @private
     */
    _showServiceError: function(sDetails) {
      let sErrorMessage = "";
      if (this._bMessageOpen) {
        return;
      }

      // try to find an error mesage in the server response
      // this would be any busy tech exception in the gateway
      try {
        const oMessageObject = JSON.parse(sDetails.responseText);
        sErrorMessage = oMessageObject.error.message.value;
      } catch (errJSON) {
        if (sDetails.statusText) {
          // HTTP errors(connection errors) fall in this section
          sErrorMessage += sDetails.statusText;

          // uncaught exceptions like nullpointer
          // come as xml..
          try {
            const oParser = new DOMParser();
            const sResponseText = oParser.parseFromString(
              sDetails.responseText,
              "text/xml"
            );
            const aMessage = sResponseText.getElementsByTagName("message");
            sErrorMessage += ":\n" + aMessage[0].textContent;
          } catch (errXML) {
            // last fallback, should`t happen error wasn?t parable at all
            if (sErrorMessage === "" || !sErrorMessage) {
              sErrorMessage = this._oResourceBundle.getText("errorGeneral");
            }
          }
        }
      }

      this._bMessageOpen = true;
      MessageBox.show(sErrorMessage, {
        icon: MessageBox.Icon.ERROR,
        id: "serviceErrorMessageBox",
        details: sDetails,
        actions: [MessageBox.Action.CLOSE],
        onClose: function() {
          this._bMessageOpen = false;
        }.bind(this)
      });
    }
  });
});
