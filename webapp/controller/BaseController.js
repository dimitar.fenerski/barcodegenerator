sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/routing/History",
    "sap/ui/model/json/JSONModel"
  ],
  function(Controller, History, JSONModel) {
    "use strict";
    return Controller.extend(
      "ch.berncon.DF.BarcodeGenerator.BarcodeGenerator.controller.BaseController",
      {
        onInit: function() {
          this._viewModel = new JSONModel();
          this.getView().setModel(this._viewModel, "view");
        },

        getRouter: function() {
          /*eslint-disable */
          return sap.ui.core.UIComponent.getRouterFor(this);
          /*eslint-enable */
        },

        onChooseView: function(oEvent) {
          if (!this._oPopover) {
            this._oPopover = sap.ui.xmlfragment(
              "ch.berncon.DF.BarcodeGenerator.BarcodeGenerator.view.ChooseView",
              this
            );
            this.getView().addDependent(this._oPopover);
          }
          this._oPopover.openBy(oEvent.getSource());
        },

        onNavBack: function() {
          const oHistory = History.getInstance();
          const sPreviousHash = oHistory.getPreviousHash();
          if (sPreviousHash !== undefined) {
            window.history.go(-1);
          } else {
            /* no history */
            this.getRouter().navTo("RouteHome", {}, true);
          }
        },

        onHome: function() {
          this.getRouter().navTo("RouteHome", {}, true);
        }
      }
    );
  }
);
