sap.ui.define(["../controller/BaseController"], function(BaseController) {
  "use strict";

  return BaseController.extend(
    "ch.berncon.DF.BarcodeGenerator.BarcodeGenerator.controller.Home",
    {
      onInit: function() {
        // BaseController.prototype.onInit.apply(this, arguments);
      },

      onView2: function(oEvent) {
        this.getRouter().navTo("RouteView2");
      }
    }
  );
});
