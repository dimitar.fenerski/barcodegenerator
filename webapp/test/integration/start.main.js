/* globals  opaTest, QUnit*/
sap.ui.require([
    "sap/ui/test/Opa5",
    "sap/ui/test/opaQunit",
    "sap/ui/test/matchers/Visible",
    "sap/ui/test/actions/Press"
], function(Opa5, opaQunit, Visible, Press) {
    "use strict";
    const sNamesapce = "ch.berncon.generator.ui5";

    Opa5.extendConfig({
        viewNamespace: sNamesapce + ".view.",
        viewName: "Main",
        autoWait: true
    });

    QUnit.module("iStartMyUIComponent");
    opaTest("Should start a component", function(Given, When, Then) {
        Given.iStartMyUIComponent({
            componentConfig: {
                name: sNamesapce
            },
            hash: ""
        });

        When.waitFor({
            viewName: "Main",
            id: "list",

            success: function(oAp) {
                Opa5.assert.ok(true, "The component was loaded");
            }
        });
        // Removes the component again
        //.and.iTeardownMyUIComponent();
    });
    opaTest("open version dialog test", function(Given, When, Then) {
        When.waitFor({
            id: "versionButton",
            // For pressing controls use the press action
            // The button is busy so OPA will automatically wait until you can press it
            actions: new Press(),
            errorMessage: "The navigation-button was not pressable"
        });

        Then.waitFor({
            id: "versionDialog",
            success: function() {
                Opa5.assert.ok(true, "open version dialog test was a success");
            },
            errorMessage: "Was not able to test dialog"
        });
    });


});
