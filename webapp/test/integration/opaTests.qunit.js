/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"ch/berncon/DF/BarcodeGenerator/BarcodeGenerator/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});