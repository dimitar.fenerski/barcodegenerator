sap.ui.define([], function() {
  "use strict";
  return {
    setBarcodeWidth: function(bPhone) {
      let sWidth = "";
      if (bPhone) {
        sWidth = "90%";
      } else {
        sWidth = "50%";
      }
      return sWidth;
    },
    toLowerCase: function(string) {
      if (string) {
        return string.toLowerCase();
      }
      return "";
    }
  };
});
