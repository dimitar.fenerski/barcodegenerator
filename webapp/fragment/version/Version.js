sap.ui.define([
    "sap/ui/base/Object"
], function(UI5Object) {
    "use strict";
    return UI5Object.extend("ch.berncon.generator.ui5.fragment.version.Version", {
        constructor: function(oController) {
            this._oView = oController.getView();
            this._sFullFragmentPath = "ch.berncon.generator.ui5.fragment.version.VersionDialog";
        },
        open: function() {
            if (!this._oDialog) {
                // create dialog lazily
                this._oDialog = sap.ui.xmlfragment(this._oView.getId(), this._sFullFragmentPath, this);
                this._oView.addDependent(this._oDialog);
            }
            this._oDialog.open();
        },
        onCloseDialog: function() {
            this._oDialog.close();
        }
    });
});
